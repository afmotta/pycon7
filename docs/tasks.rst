Tasks
=====

We have developed two automation chains: one for development, one for production.

config
------

We decide to keep all the config of the tasks together in a config.js file.
This allow us to make the tasks more reusable.

default
-------

The default task, which is called when the gulp command is called without a param, calls the dev task.

dev
---

The dev task is the access point for the dev chain. The task has two deps: runserver and watch

runserver
---------

The runserver task lanches a child process, running the default django runserver. The process uses the stdout, allowing
the normal use of the debugging tools like ipdb.
By default, the task executes django runserver on port 8010, passing the parameter --settings=<project-name>.settings.dev

watch
-----

THe watch tasks sets a three watcher on style, script and html files.
On modification, respectivelly the styles, scripts and deps tasks are run.

browsersync
-----------

Runs a browsersync wrapper around the standard runserver on port 8000. Browsersync ui runs on port 8080.

build
-----

Uses 'runSequence' module to run subtasks in strict sequence. The subtasks are, in order
  - bower
  - delete
  - scripts
  - styles
  - deps

bower
-----

Runs a bower install command in a child process.

delete
------

Deletes the content of the 'templates_dev' and 'static/dev' folders.

scripts
-------

The tasks compiles the es2015 scripts in es5 scripts and copies the results in the dest folder.

styles
------

The task compiles scss and sass stylesheets in css, then passes them through autoprefixer and mqpacker.
Finally copies the files in the dest folder.

deps
----

Installs bower dependencies and project assets in the 'templates_dev/base.html' folder.

prod
----

The prod task is the access point for the prod chain.
Uses 'runSequence' module to run subtasks in strict sequence. The subtasks are, in order:
  - bower
  - delete:prod
  - scripts
  - styles
  - optimize
  - inject

delete:prod
-----------

Deletes the content of the 'templates_prod' and 'static/prod' folders.

optimize
--------

Concats and minifies css and contacts and uglifies js, both from bower and project deps.

inject
------

Injects optimized assets files inside 'templates_prod/base.html'.